
#ifndef VISION_H
#define VISION_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>
using namespace cv;

//Valores para hilera
#define DOSHILERAS 1
#define UNAHILERA 0

bool descripcion_hilera = DOSHILERAS;

vector<Mat> canales;
#if MOSTRAR_IMAGENES
Mat pruebanuevoalgoritmo, dibujo, primeramascara, segundamascara, segundasegmentacion, eliminacioncontornos, verdearreglado, contornosiniciales;  //Nuevo algoritmo para la segmentación
#endif

std::vector<Prediction> predictions;
Mat imagen,verde,mascara,recortada,temporal;
vector<vector<Point> > contours;
vector<vector<Point> > contours_poly;
vector<Vec4i> hierarchy;
Rect recta, rectamenor;
Moments momentorecta, momentorectamenor;

vector<Point> coordenadasmaiz;
vector<Point> coordenadasmalahierba;
clock_t begin, end;

float distanciabsoluta = 0;

const int offset = 8; //30pixeles de offset
std::ifstream numerodeimagenin;
std::ofstream numerodeimagenout;
int numeroimagendebug = 0;
vector<int> compression_params;
int centrohilera = 0;   //Centro de hilera x
int area = 0;

const int lineasuperiormalahierba = 1*imagen.rows/3;
const int lineainferiormalahierba = 2*imagen.rows/3;
const float factorpx2mm = 2.5;

double tiempocontornos, tiemporeconstruir, tiempoclasificar, tiempogenerarhileras, tiempoinnecesarios;

struct objetored{
    cv::Point punto;
    int area;
    float probabilidad;
    string clase;
    int ancho;
    int largo;
};
struct posicionhilera{
    int posicionderecha1 = 1000;
    int posicionderecha2 = 0;
    int posicionizquierda1 = 1000;
    int posicionizquierda2 = 0;
    bool doshileras = true;
    bool unahileraizquierda = true;
};

posicionhilera hileraimagen; //Variable donde los datos de la rejilla de la hilera de la imagen

objetored maices;
objetored malashierbas;
std::vector<objetored> maicestotales;
std::vector<objetored> malashierbastotales;

void encontrarcontornos(){
    begin = clock();
    std::cout << "Inicio Contornos" << std::endl;

#if MOSTRAR_IMAGENES
    imagen.copyTo(dibujo);
    imagen.copyTo(pruebanuevoalgoritmo);
#endif

    split(imagen,canales);
    normalize(canales[1],canales[1],0,255,NORM_MINMAX,-1,Mat());
    verde = 2*canales[1] - canales[0] - canales[2];
    dilate(verde,verde,getStructuringElement(MORPH_CROSS,Size(7,7)));
    medianBlur(verde,verde,5);
    threshold(verde,verde,27,255,THRESH_BINARY | THRESH_OTSU);
    findContours( verde, contours, hierarchy, CV_RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(0, 0) );
#if MOSTRAR_IMAGENES
    imagen.copyTo(primeramascara,verde);
    imagen.copyTo(contornosiniciales);
    imshow("Primera Segmentacion",verde);
    imshow("Primera mascara",primeramascara);
    primeramascara.release();
    contours_poly.resize(contours.size());
    std::cout << "Contornos iniciales: " << contours.size() << std::endl;
    for( size_t i = 0; i < contours.size(); i++ ){
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
        recta = boundingRect(Mat(contours_poly[i]) );
        area = contourArea(contours[i]);
        if(area < 5000) rectangle(contornosiniciales,recta,Scalar(25,25,255),4);
        else{
            Point distanciarecta;
            Point centrodegravedad;
            momentorecta = moments( contours[i], false );
            centrodegravedad.x = momentorecta.m10/momentorecta.m00;
            centrodegravedad.y = momentorecta.m01/momentorecta.m00;
            rectangle(contornosiniciales,recta,Scalar(25,255,25),4);
            circle(contornosiniciales, centrodegravedad, 5, Scalar(255,255,140),-1);
        }
        drawContours(contornosiniciales, contours, i, Scalar(255,255,255));
        std::cout << "Contorno [" << i << "]: Area: " << area << "  (" << recta.x << "," << recta.y << ")   Ancho: " << recta.width << "    Largo:" << recta.height << std::endl;
    }
    imshow("Contornos iniciales", contornosiniciales);
    contours_poly.clear();
#endif
    end = clock();
    tiempocontornos = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Tiempo deteccion contornos: " << tiempocontornos << std::endl;
    std::cout << "Tamaño contornos reconstruidos: " << contours.size() << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << "Fin deteccion contornos" << std::endl;
    std::cout << "=================================" << std::endl;
}

void reconstruircontornos(){

    begin = clock();
    //borrar luego de hacer debug
    Mat pruebareconstruircontornos;
#if MOSTRAR_IMAGENES
    verdearreglado = Mat(imagen.rows, imagen.cols, CV_8UC1,Scalar(0));
#endif
    contours_poly.resize(contours.size());
    for( size_t i = 0; i < contours.size(); i++ ){
        area = contourArea(contours[i]);
        //std::cout << "Area[" << i << "]: " << area  <<  std::endl;
        if(area < 25){  //Elimina los contornos pequeños antes de entrar a la red
            drawContours(verde,contours,i,Scalar(0),-1);
            contours.erase(contours.begin() + i);
            //contours_poly.erase(contours_poly.begin() + i);
            i -=1 ;
        }
        else{
            approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
            recta = boundingRect(Mat(contours_poly[i]) );
            //std::cout << "Recta[" << i << "]: " << "(" << recta.x << "," << recta.y << ")   " << "recta ancho: " << recta.width << "    recta largo: " << recta.height  << std::endl;
            for( size_t j = 0; j < contours.size(); j++ ){
                if(i != j){
                    approxPolyDP( Mat(contours[j]), contours_poly[j], 3, true );
                    rectamenor = boundingRect(Mat(contours_poly[j]) );
                     float distanciaentrecontornos = 0;
                    if(area > 5000){
                        float distanciaminima = 2000;
                        float distancia = 0;
                        Point puntocercano1, puntocercano2, puntoprueba1, puntoprueba2;
                        int posicion; //0 esquina superior izquierda, 1 esquiza superior derecha, 2 esquina inferior izquierda, 3 esquina inferior derecha
                        //Busco en que cuadrantes opuestos se encuentran
                        if((recta.x + recta.width/2 > rectamenor.x+ rectamenor.width/2 )  & (recta.y + recta.height/2 > rectamenor.y+ rectamenor.height/2)) {
                            posicion = 0;
                            distanciaentrecontornos = norm(Point(recta.x, recta.y)-Point(rectamenor.x + rectamenor.width, rectamenor.y + rectamenor.height));
                        }
                        else if((recta.x + recta.width/2 < rectamenor.x+ rectamenor.width/2 )  & (recta.y + recta.height/2 > rectamenor.y+ rectamenor.height/2)) {
                            posicion = 1;
                            distanciaentrecontornos = norm(Point(recta.x + recta.width, recta.y)-Point(rectamenor.x, rectamenor.y + rectamenor.height));
                        }
                        else if((recta.x + recta.width/2 > rectamenor.x+ rectamenor.width/2 )  & (recta.y + recta.height/2 < rectamenor.y+ rectamenor.height/2)){
                            posicion = 2;
                            distanciaentrecontornos = norm(Point(recta.x, recta.y + recta.height) - Point(rectamenor.x + rectamenor.width, rectamenor.y));
                        }
                        else if((recta.x + recta.width/2 < rectamenor.x+ rectamenor.width/2 )  & (recta.y + recta.height/2 < rectamenor.y+ rectamenor.height/2)) {
                            posicion = 3;
                            distanciaentrecontornos = norm(Point(recta.x + recta.width, recta.y + recta.height)- Point(rectamenor.x, rectamenor.y));
                        }
                        else{
                            distanciaentrecontornos = 1000;
                            std::cout << "No entro?" << std::endl;
                            std::cout << "Recta: {" << recta.x  << "," << recta.y << "}" << std::endl;
                            std::cout << "Recta menor: {" << rectamenor.x  << "," << rectamenor.y << "}" << std::endl;
                        }
                        std:: cout << "Distancia entre contornos: " << distanciaentrecontornos << std::endl;
                        bool calculop1 = false, calculop2 = false;

                        if(distanciaentrecontornos < 200){
                            bool unido = false;
                            std::cout << "Distancia entre rectangulos: " << distanciaentrecontornos << std::endl;
                            for(size_t a = 0; a < contours.at(i).size(); a++){  //Verifica cada punto de aca
                                if(unido) break;
                                pruebareconstruircontornos.release();
                                imagen.copyTo(pruebareconstruircontornos);
                                for(size_t b = 0; b < contours.at(j).size(); b++){
                                    if(posicion == 0){
                                        if((contours.at(j).at(b).x < recta.x + recta.width/2) & (contours.at(j).at(b).y < recta.y + recta.height/2) ) {
                                            puntoprueba1.x = contours.at(j).at(b).x;
                                            puntoprueba1.y = contours.at(j).at(b).y;
                                            calculop1 = true;
                                        }
                                        else calculop1 = false;
                                        if((contours.at(i).at(a).x < recta.x + recta.width/2) & (contours.at(i).at(a).y < recta.y + recta.height/2) ) {
                                            puntoprueba2.x = contours.at(i).at(a).x;
                                            puntoprueba2.y = contours.at(i).at(a).y;
                                            calculop2 = true;
                                        }
                                        else calculop2 = false;
                                    }
                                    else if(posicion == 1){
                                        if((contours.at(j).at(b).x > recta.x + recta.width/2) & (contours.at(j).at(b).y < recta.y + recta.height/2) ) {
                                            puntoprueba1.x = contours.at(j).at(b).x;
                                            puntoprueba1.y = contours.at(j).at(b).y;
                                            calculop1 = true;
                                        }
                                        else calculop1 = false;
                                        if((contours.at(i).at(a).x > recta.x + recta.width/2) & (contours.at(i).at(a).y < recta.y + recta.height/2) ) {
                                            puntoprueba2.x = contours.at(i).at(a).x;
                                            puntoprueba2.y = contours.at(i).at(a).y;
                                            calculop2 = true;
                                        }
                                        else calculop2 = false;

                                    }
                                    else if(posicion == 2){
                                        if((contours.at(j).at(b).x < recta.x + recta.width/2) & (contours.at(j).at(b).y > recta.y + recta.height/2) ) {
                                            puntoprueba1.x = contours.at(j).at(b).x;
                                            puntoprueba1.y = contours.at(j).at(b).y;
                                            calculop1 = true;
                                        }
                                        else calculop1 = false;
                                        if((contours.at(i).at(a).x < recta.x + recta.width/2) & (contours.at(i).at(a).y > recta.y + recta.height/2) ) {
                                            puntoprueba2.x = contours.at(i).at(a).x;
                                            puntoprueba2.y = contours.at(i).at(a).y;
                                            calculop2 = true;
                                        }
                                        else calculop2 = false;

                                    }
                                    else if(posicion == 3){
                                        if((contours.at(j).at(b).x > recta.x + recta.width/2) & (contours.at(j).at(b).y > recta.y + recta.height/2) ) {
                                            puntoprueba1.x = contours.at(j).at(b).x;
                                            puntoprueba1.y = contours.at(j).at(b).y;
                                            calculop1 = true;
                                        }
                                        else calculop1 = false;
                                        if((contours.at(i).at(a).x > recta.x + recta.width/2) & (contours.at(i).at(a).y > recta.y + recta.height/2) ) {
                                            puntoprueba2.x = contours.at(i).at(a).x;
                                            puntoprueba2.y = contours.at(i).at(a).y;
                                            calculop2 = true;
                                        }
                                        else calculop2 = false;

                                    }
                                    if(calculop1 & calculop2){
                                        circle(pruebareconstruircontornos,puntoprueba1,5, Scalar(0,0,255),-1);
                                        circle(pruebareconstruircontornos,puntoprueba2,5, Scalar(0,0,255),-1);

                                        //imshow("PruebaRcontornos", pruebareconstruircontornos);
                                        //waitKey();

                                        distancia = norm(puntoprueba2-puntoprueba1);
                                        if(distancia < distanciaminima){
                                            distanciaminima = distancia;
                                            puntocercano1 = puntoprueba1;
                                            puntocercano2 = puntoprueba2;
                                            if(distanciaminima < 20){
                                                std::cout << "Distancia minima: " << distanciaminima << std::endl;
                                                line(verde,puntocercano1,puntocercano2,Scalar(255),12);
                                                unido = true;
                                                break;

                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                    else{
                        //std::cout << "Por implementar" << std::endl;
                    }
                    //Codigo antiguo
                    /*if(area > 2000){       //Trata de unir contornos mayores con menores que esten cercanos
                        float radio = 0;
                        bool encontrado = false;
                        if((recta.x + recta.width/2) > (rectamenor.x + rectamenor.width/2)){
                            if((recta.x - offset < rectamenor.x + rectamenor.width) & (recta.x + recta.width/2 > rectamenor.x + rectamenor.width) & (recta.y - offset < rectamenor.y + rectamenor.height) & (recta.y + recta.height + offset > rectamenor.y + rectamenor.height)){
                                encontrado = true;
                            }
                        }
                        else{
                            if((recta.x + recta.width + offset > rectamenor.x) & (recta.x + recta.width/2 < rectamenor.x) & (recta.y - offset < rectamenor.y + rectamenor.height) & (recta.y + recta.height + offset > rectamenor.y + rectamenor.height)){
                                encontrado = true;
                            }
                        }
                        //std:: cout << "Distancia absoluta: " << distanciabsoluta << std::endl;
                        if(encontrado) {
                            line(verde, Point((momentorecta.m10/momentorecta.m00), (momentorecta.m01/momentorecta.m00)), Point((momentorectamenor.m10/momentorectamenor.m00), (momentorectamenor.m01/momentorectamenor.m00)),Scalar(255),20);
                            std::cout << "Distancia: " << distanciabsoluta << std::endl;
                            std::cout << "Recta menor[" << j << "]: " << "(" << rectamenor.x << "," << rectamenor.y << ")   " << "recta ancho: " << rectamenor.width << "    recta largo: " << rectamenor.height  << std::endl;
                            std::cout << "Recta[" << i << "]: " << "(" << recta.x << "," << recta.y << ")   " << "recta ancho: " << recta.width << "    recta largo: " << recta.height  << std::endl;
                            //std::cout << "Distanciax: " << distanciax << "  Distanciay: " << distanciay << std::endl;
                        }
                    }*/
                }
            }
        }
    }
    contours.clear();
    contours_poly.clear();
    findContours( verde, contours, hierarchy, CV_RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(0, 0) );
#if MOSTRAR_IMAGENES
    contours_poly.resize(contours.size());
    std::cout << "Contornos finales: " << contours.size() << std::endl;
    for( size_t i = 0; i < contours.size(); i++ ){
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
        recta = boundingRect(Mat(contours_poly[i]) );
        drawContours(verdearreglado,contours,i,Scalar(255),-1,LINE_AA);
        area = contourArea(contours[i]);
        std::cout << "Contorno [" << i << "]: Area: " << area << "  (" << recta.x << "," << recta.y << ")   Ancho: " << recta.width << "    Largo:" << recta.height << std::endl;
    }
    imshow("Segunda segmentacion",verde);
    imagen.copyTo(segundamascara, verde);
    imshow("Segunda mascara",segundamascara);
    segundamascara.release();
    contours_poly.clear();
#endif
    end = clock();
    tiemporeconstruir = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Tiempo reconstruir contornos: " << tiemporeconstruir << std::endl;
    std::cout << "Tamaño contornos reconstruidos: " << contours.size() << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << "Fin reconstruccion contornos" << std::endl;
    std::cout << "=================================" << std::endl;
}

void clasificar(Classifier classifier){
    begin = clock();
    std::cout << "Inicio clasificador" << std::endl;
    contours_poly.resize(contours.size());
    for( size_t i = 0; i < contours.size(); i++ ){
        area = contourArea(contours[i]);
        temporal = Mat(imagen.rows, imagen.cols, CV_8UC1,Scalar(0));
        mascara = Mat(imagen.rows, imagen.cols, CV_8UC1,Scalar(0));
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
        recta = boundingRect( Mat(contours_poly[i]) );
#if MOSTRAR_IMAGENES
        rectangle(dibujo,recta,Scalar(125,255,125),5);
#endif
        drawContours(temporal,contours,i,Scalar(255),-1,LINE_AA);
        imagen.copyTo(mascara,temporal);

        if(area >= 4096){
            if((recta.y > 0) & (recta.y   <   imagen.rows)){
                recortada = mascara(recta);
                predictions = classifier.Classify(recortada);
                Prediction p = predictions[0];
                //std::cout << std::fixed << std::setprecision(4) << p.second << " - \"" << p.first << "\"" << std::endl;
                //std::cout << ".:===========:." << std::endl;
                if((p.second > 0.9) & (p.first == "maiz")) {
#if MOSTRAR_IMAGENES
                    rectangle(dibujo,recta,Scalar(0,255,0),4);
#endif

                    //putText(dibujo,"Maiz" + std::to_string(p.second),Point(recta.x,recta.y),FONT_HERSHEY_PLAIN,2.0,Scalar(0,255,255),3);
                    maices.area = area;
                    maices.clase = "maiz";
                    maices.ancho = recta.width;
                    maices.largo = recta.height;
                    maices.punto = Point(recta.x,recta.y);
                    maices.probabilidad = p.second;
                    maicestotales.push_back(maices);
                }
                else if((p.second > 0.5) & (p.first == "malahierba ")){
                    malashierbas.area = area;
                    malashierbas.clase = "malahierba";
                    malashierbas.ancho = recta.width;
                    malashierbas.largo = recta.height;
                    malashierbas.punto = Point(recta.x,recta.y);
                    malashierbas.probabilidad = p.second;
                    std::cout << 600-recta.y/2.13 << std::endl;
                    if(600-recta.y/2.13 > 100) coordenadasmalahierba.push_back(Point(600-recta.x/2.13,600-recta.y/2.13));
#if MOSTRAR_IMAGENES
                    if(recta.width > recta.height) {
                        circle(dibujo,Point(recta.x + recta.width/2, recta.y + recta.height/2),recta.width/2,Scalar(0,255,255),4);
                        putText(dibujo,"x: " + std::to_string((int)600-recta.x/2.13) + " y:"+   std::to_string((int)600-recta.y/2.13), Point(recta.x,recta.y),FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255),2);
                    }
                    else{
                        circle(dibujo,Point(recta.x + recta.width/2, recta.y + recta.height/2),recta.height/2,Scalar(0,255,255),4);
                        putText(dibujo,"x: " + std::to_string((int)600-recta.x/2.13) + " y:"+   std::to_string((int)600-recta.y/2.13), Point(recta.x,recta.y),FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255),2);
                    }
#endif

                    if((recta.y > lineasuperiormalahierba) & (recta.y + recta.height < lineainferiormalahierba)) {
                        //  if(unavez == false) propiedadesmalahierba.push_back(recta);
                    }
                    malashierbastotales.push_back(malashierbas);
                }
            }

        }
        else{
            malashierbas.area = area;
            malashierbas.clase = "malahierba";
            malashierbas.ancho = recta.width;
            malashierbas.largo = recta.height;
            malashierbas.punto = Point(recta.x,recta.y);
            malashierbas.probabilidad = 100;
            malashierbastotales.push_back(malashierbas);
            if(600-recta.y/2.13 > 100) coordenadasmalahierba.push_back(Point(600-recta.x/2.13, 600 - recta.y/2.13));
#if MOSTRAR_IMAGENES
            circle(dibujo,Point(recta.x + recta.width/2, recta.y + recta.height/2),recta.height/2,Scalar(0,0,255),4);
            putText(dibujo,"x: " + std::to_string((int)600-recta.x/2.13) + " y:"+   std::to_string((int)600-recta.y/2.13), Point(recta.x,recta.y),FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255),2);
#endif
            /* if(recta.y > lineasuperiormalahierba & recta.y + recta.height < lineainferiormalahierba) {
                    if(unavez == false) propiedadesmalahierba.push_back(recta);
                }*/
        }

    }
#if MOSTRAR_IMAGENES
    imshow("Dibujo", dibujo);
#endif
    end = clock();
    tiempoclasificar = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Tiempo clasificar: " << tiempoclasificar << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << "Fin Clasificador" << std::endl;
    std::cout << "=================================" << std::endl;
}

void generarhilera(){
    /*
     * Funcion que saca el cento de la hilera al evaluar los puntos maximos y minimos y separar en dos
     * clases izquieda y derecha
     */
    begin = clock();
    if(maicestotales.size() == 0){
#if MOSTRAR_IMAGENES
        putText(pruebanuevoalgoritmo,"No hay maiz", Point(60,60),CV_FONT_HERSHEY_COMPLEX_SMALL ,2,Scalar(0,0,255));
#endif
    }
    if(malashierbastotales.size() == 0){
#if MOSTRAR_IMAGENES
        putText(pruebanuevoalgoritmo,"No hay mala hierba", Point(60,60),CV_FONT_HERSHEY_COMPLEX_SMALL,2,Scalar(255,0,0));
#endif
    }

    hileraimagen.posicionderecha1 = imagen.cols;
    hileraimagen.posicionderecha2 = 0;
    hileraimagen.posicionizquierda1 = imagen.cols;
    hileraimagen.posicionizquierda2 = 0;
    int mayorx = 0;
    int menorx = imagen.cols;
    int puntointermedio = 0;

    for(size_t numeroimagenesmaiz = 0; numeroimagenesmaiz < maicestotales.size(); numeroimagenesmaiz++){
        //Sacar el punto con un maiz reconocido mas a la derecha en x de la imagen
        if(maicestotales.at(numeroimagenesmaiz).punto.x > mayorx ){
            mayorx = maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2;
        }
        if(maicestotales.at(numeroimagenesmaiz).punto.x < menorx ){
            menorx = maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2;
        }

    }
    std::cout << "mayorx: " << mayorx << std::endl;
    std::cout << "menorx: " << menorx << std::endl;
    std::cout << "puntointermedio: " << (mayorx + menorx)/2 << std::endl;
    float relacionposicion;
    if(menorx > 0){
        relacionposicion = (float)mayorx/menorx;
    }
    else
    {
        relacionposicion = (float)mayorx/0.1;
    }

    std::cout << "Relacion de posicion: " << relacionposicion << std::endl;
    if(relacionposicion > 1.8){ //Revisa si la distancia entre los puntos extremos es muy cercana
        std::cout << "Entra a dos hileras" << std::endl;
        hileraimagen.doshileras =  DOSHILERAS;
#if MOSTRAR_IMAGENES
        circle(dibujo, Point((mayorx + menorx)/2, imagen.rows/2),20,Scalar(0,0,0),5);
#endif
        puntointermedio = (mayorx + menorx)/2;
        std::vector<cv::Point> hileraderecha;
        std::vector<cv::Point> hileraizquierda;
        std::cout << "Crear vector derecho e izquierdo" << std::endl;
        for(size_t numeroimagenesmaiz = 0; numeroimagenesmaiz < maicestotales.size(); numeroimagenesmaiz++){
            //Sacar el punto con un maiz reconocido mas a la derecha en x de la imagen
            if(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2 > puntointermedio){
                hileraderecha.push_back(Point(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2,maicestotales.at(numeroimagenesmaiz).punto.y));
                if(maicestotales.at(numeroimagenesmaiz).punto.x < hileraimagen.posicionderecha1){
                    hileraimagen.posicionderecha1 = maicestotales.at(numeroimagenesmaiz).punto.x;
                }
                if(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho > hileraimagen.posicionderecha2){
                    hileraimagen.posicionderecha2 = maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho;
                }
            }
            else{
                hileraizquierda.push_back(Point(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2,maicestotales.at(numeroimagenesmaiz).punto.y));
                if(maicestotales.at(numeroimagenesmaiz).punto.x < hileraimagen.posicionizquierda1){
                    hileraimagen.posicionizquierda1 = maicestotales.at(numeroimagenesmaiz).punto.x;
                }
                if(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho > hileraimagen.posicionizquierda2){
                    hileraimagen.posicionizquierda2 = maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho;
                }
            }
        }
        std::cout << "Reconoce los dos tipos de hileras" << std::endl;
        int centroderecha = 0;
        for(size_t i = 0; i < hileraderecha.size(); i++){
            std::cout << "Punto Derecha {" << i << "}: " << hileraderecha.at(i) << std::endl;
            centroderecha += hileraderecha.at(i).x;
        }
        centroderecha/=hileraderecha.size();
        int centroizquierda = 0;
        for(size_t i = 0; i < hileraizquierda.size(); i++){
            std::cout << "Punto Izquierda {" << i << "}: " << hileraizquierda.at(i) << std::endl;
            centroizquierda += hileraizquierda.at(i).x;
        }
        centroizquierda/=hileraizquierda.size();
        std::cout << "Centroizquierda: " << centroizquierda << std::endl;
        std::cout << "Centroderecha: " << centroderecha << std::endl;
        std::cout << "PuntoIntermediofinal: " << (centroizquierda + centroderecha)/2 << std::endl;
        centrohilera = (centroizquierda + centroderecha)/2;
#if MOSTRAR_IMAGENES
        circle(pruebanuevoalgoritmo, Point(centrohilera, imagen.rows/3 - 30),20,Scalar(0,0,0),5);
        line(pruebanuevoalgoritmo,Point(centrohilera,imagen.rows/3 - 25 - 30), Point(centrohilera,imagen.rows/3  - 30 + 25),Scalar(0,0,0),5);
        line(pruebanuevoalgoritmo,Point(centrohilera-25,imagen.rows/3 - 30), Point(centrohilera+25,imagen.rows/3 - 30),Scalar(0,0,0),5);
#endif


    }
    else{
        std::cout << "Entra a una hilera" << std::endl;
        hileraimagen.doshileras = UNAHILERA;
        std::vector<cv::Point> hileracentro;
        for(size_t numeroimagenesmaiz = 0; numeroimagenesmaiz < maicestotales.size(); numeroimagenesmaiz++){
            //Todavia no se sabe donde estara el punto
            hileracentro.push_back(Point(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho/2,maicestotales.at(numeroimagenesmaiz).punto.y));
            if(maicestotales.at(numeroimagenesmaiz).punto.x < hileraimagen.posicionizquierda1){
                hileraimagen.posicionizquierda1 = maicestotales.at(numeroimagenesmaiz).punto.x;
            }
            if(maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho > hileraimagen.posicionizquierda2){
                hileraimagen.posicionizquierda2 = maicestotales.at(numeroimagenesmaiz).punto.x + maicestotales.at(numeroimagenesmaiz).ancho;
            }
        }
        std::cout << "Saca los puntos de una hilera" << std::endl;
        hileraimagen.posicionderecha1 = 0;
        hileraimagen.posicionderecha2 = 0;

        int centro = 0;
        for(size_t i = 0; i< hileracentro.size(); i++){
            centro += hileracentro.at(i).x;
        }
        if(hileracentro.size() > 0) centro/= hileracentro.size();
        else centro/= 1;
        if(centro < imagen.cols/2) {
            centrohilera = (centro + imagen.cols)/2;
            hileraimagen.unahileraizquierda = false;
            std::cout << "Centor hilera x: " << centrohilera <<  std::endl;
        }
        else {
            centrohilera = (centro)/2;
            hileraimagen.unahileraizquierda = true;
            std::cout << "Centor hilera x: " << centrohilera <<  std::endl;
        }
#if MOSTRAR_IMAGENES
        circle(pruebanuevoalgoritmo, Point(centrohilera, imagen.rows/3 - 30),20,Scalar(0,0,0),5);
        line(pruebanuevoalgoritmo,Point(centrohilera,imagen.rows/3 - 25 - 30), Point(centrohilera,imagen.rows/3  - 30 + 25),Scalar(0,0,0),5);
        line(pruebanuevoalgoritmo,Point(centrohilera-25,imagen.rows/3 - 30), Point(centrohilera+25,imagen.rows/3 - 30),Scalar(0,0,0),5);
#endif

    }
    if(hileraimagen.doshileras){
        std::cout << "Dos hileras" << std::endl;
        std::cout << "Posicion izquierda1   " << hileraimagen.posicionizquierda1 << std::endl;
        std::cout << "Posicion izquierda2   " << hileraimagen.posicionizquierda2 << std::endl;
        std::cout << "Posicion derecha1 " << hileraimagen.posicionderecha1 << std::endl;
        std::cout << "Posicion derecha2 " << hileraimagen.posicionderecha2 << std::endl;
#if MOSTRAR_IMAGENES
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionizquierda1,0),Point(hileraimagen.posicionizquierda1,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionizquierda2,0),Point(hileraimagen.posicionizquierda2,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionderecha1,0),Point(hileraimagen.posicionderecha1,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionderecha2,0),Point(hileraimagen.posicionderecha2,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        line(pruebanuevoalgoritmo, Point(hileraimagen.posicionizquierda2,pruebanuevoalgoritmo.rows/3), Point(hileraimagen.posicionderecha1,pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
        line(pruebanuevoalgoritmo, Point(hileraimagen.posicionizquierda2,2*pruebanuevoalgoritmo.rows/3), Point(hileraimagen.posicionderecha1,2*pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
#endif


    }
    else{
        std::cout << "Una hileras" << std::endl;
        std::cout << "Posicion izquierda1   " << hileraimagen.posicionizquierda1 << std::endl;
        std::cout << "Posicion izquierda2   " << hileraimagen.posicionizquierda2 << std::endl;
#if MOSTRAR_IMAGENES
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionizquierda1,0),Point(hileraimagen.posicionizquierda1,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        line(pruebanuevoalgoritmo,Point(hileraimagen.posicionizquierda2,0),Point(hileraimagen.posicionizquierda2,pruebanuevoalgoritmo.rows),Scalar(30,0,200),10);
        if(!hileraimagen.unahileraizquierda){
            line(pruebanuevoalgoritmo, Point(hileraimagen.posicionizquierda2,pruebanuevoalgoritmo.rows/3), Point(imagen.cols,pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
            line(pruebanuevoalgoritmo, Point(hileraimagen.posicionizquierda2,2*pruebanuevoalgoritmo.rows/3), Point(pruebanuevoalgoritmo.cols,2*pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
        }
        else{
            line(pruebanuevoalgoritmo, Point(0,pruebanuevoalgoritmo.rows/3), Point(hileraimagen.posicionizquierda1,pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
            line(pruebanuevoalgoritmo, Point(0,2*pruebanuevoalgoritmo.rows/3), Point(hileraimagen.posicionizquierda1,2*pruebanuevoalgoritmo.rows/3),Scalar(255,255,255),3);
        }
#endif


    }
#if MOSTRAR_IMAGENES
    imshow("PNA",pruebanuevoalgoritmo);
#endif
    end = clock();
    tiempogenerarhileras = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Tiempo generacion hilera: " << tiempogenerarhileras << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << "Finaliza generacion de hilera" << std::endl;
    std::cout << "=================================" << std::endl;
}
void eliminarinnecesarios(){
    begin = clock();
    for(size_t j = 0; j < malashierbastotales.size(); j++){
        if(hileraimagen.doshileras){
            if((malashierbastotales.at(j).punto.x < hileraimagen.posicionizquierda2) | (malashierbastotales.at(j).punto.x + malashierbastotales.at(j).ancho  > hileraimagen.posicionderecha1)){
                malashierbastotales.erase(malashierbastotales.begin() + j);
                j -= 1;
            }
        }
        else{
            if(hileraimagen.unahileraizquierda){
                if(malashierbastotales.at(j).punto.x > hileraimagen.posicionizquierda1){
                    malashierbastotales.erase(malashierbastotales.begin() + j);
                    j -= 1;
                }
            }
            else{
                if(malashierbastotales.at(j).punto.x < hileraimagen.posicionizquierda2){
                    malashierbastotales.erase(malashierbastotales.begin() + j);
                    j -= 1;
                }
            }

        }
        /*if(malashierbastotales.at(j).largo* malashierbastotales.at(j).ancho < 2*offset){            //Eliminar malas hierbas pequeñas
            malashierbastotales.erase(malashierbastotales.begin() + j);
            j -= 1;
        }*/
    }
    for(size_t i = 0; i < malashierbastotales.size(); i++){
        if((malashierbastotales.at(i).punto.y < imagen.rows/3) | (malashierbastotales.at(i).punto.y + malashierbastotales.at(i).largo > 2*imagen.rows/3) ){
            malashierbastotales.erase(malashierbastotales.begin() + i);
            i -= 1;
        }
    }

    for(size_t i = 0; i < maicestotales.size(); i++){
        recta = Rect(maicestotales.at(i).punto.x, maicestotales.at(i).punto.y, maicestotales.at(i).ancho,  maicestotales.at(i).largo);
#if MOSTRAR_IMAGENES
        rectangle(pruebanuevoalgoritmo,recta,Scalar(0,255,0),4);
#endif

    }
    for(size_t i = 0; i < malashierbastotales.size(); i++){
#if MOSTRAR_IMAGENES
        circle(pruebanuevoalgoritmo,Point(malashierbastotales.at(i).punto.x + malashierbastotales.at(i).ancho/2, malashierbastotales.at(i).punto.y + malashierbastotales.at(i).largo/2),malashierbastotales.at(i).largo/2,Scalar(0,255,255),4);
#endif
    }
#if MOSTRAR_IMAGENES
    imshow("Prueba nuevo algoritmo", pruebanuevoalgoritmo);
#endif
    end = clock();
    tiempoinnecesarios = (double)(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Tiempo deteccion contornos: " << tiempocontornos << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << "Finaliza eliminar innecesarios" << std::endl;
    std::cout << "=================================" << std::endl;
}
void restablecer(){
    contours.clear();
    contours_poly.clear();
    maicestotales.clear();
    malashierbastotales.clear();
}
void inicializardebug(){
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    /*numerodeimagenin.open("numeroimagen.txt");
    std::string line;
    if (numerodeimagenin.is_open())
      {
        std::cout << "Abriendo archivo de texto" << std::endl;
        while ( std::getline(numerodeimagenin,line) )
            {
              std::cout << "Linea: " << line << '\n';
            }
        //std::getline(numerodeimagenin,line);
        //std::cout << "Numero imagen debug: " << line << std::endl;
        //numeroimagendebug = std::stoi(line);
        numerodeimagenin.close();
      }
    else{
        std::cout << "Creando un documento de texto" << std::endl;
        numerodeimagenout.open("numeroimagen.txt");
        numerodeimagenout << "0";
    }*/

}
void restablecerdebug(){
    numerodeimagenout.close();
}
void guardarimagen(Mat imagenrequerida, String nombre){
    imwrite(nombre + "_" + std::to_string(numeroimagendebug) +  ".png", imagenrequerida, compression_params);
}

#endif // VISION_H
