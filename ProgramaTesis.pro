TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

unix:!macx: LIBS += -L$$PWD/../../../../usr/local/lib/ -lopencv_highgui -lopencv_core -lopencv_imgproc -lopencv_video -lopencv_features2d -lopencv_videoio -lopencv_imgcodecs -lopencv_dnn
INCLUDEPATH += $$PWD/../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../usr/local/include


unix:!macx: LIBS += -L$$PWD/../../../../usr/local/cuda-8.0/lib64/ -lcudart -ldl -lpthread -lrt

INCLUDEPATH += $$PWD/../../../../usr/local/cuda-8.0/lib64
DEPENDPATH += $$PWD/../../../../usr/local/cuda-8.0/lib64

unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../usr/local/cuda-8.0/lib64/libcudart_static.a

unix:!macx: LIBS += -L$$PWD/../../../../usr/local/lib/ -lopencv_cudaarithm -lopencv_cudabgsegm -lopencv_features2d  -lopencv_cudafilters -lopencv_cudaimgproc -lopencv_cudalegacy -lopencv_cudaobjdetect -lopencv_cudaoptflow -lopencv_cudastereo -lopencv_cudawarping -lopencv_videoio

INCLUDEPATH += $$PWD/../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../usr/local/include


INCLUDEPATH += $$PWD/../../../../usr/local/cuda-8.0/include

INCLUDEPATH += $$PWD/../../../../usr/include/



unix:!macx: LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/ -lglog -lboost_system

INCLUDEPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu


unix:!macx: LIBS += -L$$PWD/../../Downloads/caffe-1.0/build/lib/ -lcaffe

INCLUDEPATH += $$PWD/../../Downloads/caffe-1.0/build/lib
DEPENDPATH += $$PWD/../../Downloads/caffe-1.0/build/lib

unix:!macx: LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/ -lpthread

INCLUDEPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu

HEADERS += \
    DeepLearning.h \
    GPIOSerial.h \
    Vision.h
