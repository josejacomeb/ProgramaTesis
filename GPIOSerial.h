#ifndef GPIOSERIAL_H
#define GPIOSERIAL_H

#include <wiringPi.h>
#include <wiringSerial.h>

#define pulsoentrada 0  //17 GPIO0
bool inicio = false;

int fd;

bool inicializar(){
    /*
     * Funcion que sirve para inicilizar las entradas
     * y salidas digitales, además de inicializar el
     * puerto serial
     *
    */
    wiringPiSetup();                    //Inicializa con el esquema de Wiringpi de Gordon
    pinMode(pulsoentrada, INPUT);
    pullUpDnControl (pulsoentrada, PUD_UP) ;
    //Codigo Serial

    if ((fd = serialOpen ("/dev/ttyUSB0", 115200)) < 0)
    {
        fprintf (stderr, "No se puede iniciar el dispositivo serial: %s\n", strerror (errno)) ;
        return false;
    }
    else {
        std::cout << "Inicio Serial" << std::endl;
        return true;
    }
}
bool pulsoinicio(){
    /*
     * Espera a que se de un pulso de inicio para comenzar
    */

    std::cout << "Pulse el Boton  de inicio" << std::endl;
    if(digitalRead(pulsoentrada) == HIGH){
        serialPutchar(fd,'i'); //Inicio de Ciclo
        std::cout << "Pulsado de Boton inicio" << std::endl;
        if(inicio = true) inicio = false;
        else inicio = true;
        return true;
    }
    else{
        return false;
    }

}
void mandardatos(){
    /*
     * Funcion para enviar los datos a través del puerto serial
     */
    std::cout << "Finaliza procesamiento" << std::endl;
    std::cout << "Plantas de Maiz: " << contadormaiz << std::endl;
    std::cout << "Plantas de Mala Hierba: " << contadormalahierba << std::endl;
    std::cout << "Planta de Mala Hierba Vector: " << coordenadasmalahierba.size() << std::endl;
    serialPutchar(fd,'o'); //Inicio de Transmision
    while(serialGetchar(fd)!= 'a'){}
    std::cout << "Aceptada Comunicacion Arduino" << std::endl;
    int numeromalahierbamandar = coordenadasmalahierba.size();
    salidanumeromh[0] = '0' + numeromalahierbamandar/100;
    salidanumeromh[1] = '0' + numeromalahierbamandar%100/10;
    salidanumeromh[2] = '0' + numeromalahierbamandar%100%10;
    std::cout << "Numero Mala hierba: " << salidanumeromh << std::endl;
    serialPuts(fd,salidanumeromh);
    while(serialGetchar(fd)!= 'a'){}
    for(size_t i = 0; i< coordenadasmalahierba.size(); i++){
        salidadato[0] = '0' + coordenadasmalahierba.at(i).x/100;
        salidadato[1] = '0' + coordenadasmalahierba.at(i).x%100/10;
        salidadato[2] = '0' + coordenadasmalahierba.at(i).x%100%10;
        salidadato[3] = '0' + coordenadasmalahierba.at(i).y/100;
        salidadato[4] = '0' + coordenadasmalahierba.at(i).y%100/10;
        salidadato[5] = '0' + coordenadasmalahierba.at(i).y%100%10;
        std::cout << "Dato: " << salidadato << "    " << "[" << i << "]" << std::endl;
        std::cout << "px: " << coordenadasmalahierba.at(i).x << "     " << "py: " << coordenadasmalahierba.at(i).y << std::endl;
        serialPuts(fd,salidadato);
        while(serialGetchar(fd)!= 'a'){}
        std::cout << "Nuevo Dato" << std::endl;
    }
}

bool confirmacionnavegacion(){
    while(serialGetchar(fd)!= 'f'){} // Finalizo el movimiento del robot diferencial
    return true;
}
bool confirmaciondelta(){
    while(serialGetchar(fd)!= 'd'){} // Finalizo el movimiento del robot delta
    return true;
}

#endif // GPIO_H
