#include "DeepLearning.h"
//#include "GPIOSerial.h"
#include "Vision.h"




int main(int argc, char** argv) {

    /*
    *   Inicio del clasificador
    */
    ::google::InitGoogleLogging(argv[0]);
    /*
    string model_file   = "/home/pi/Tesis/cNETmenosFiltros/deploy.prototxt";
    string trained_file = "/home/pi/Tesis/cNETmenosFiltros/maizADAM_iter_4500.caffemodel";
    string mean_file    = "/home/pi/Tesis/cNETmenosFiltros/imagenet_mean.binaryproto";
    string label_file   = "/home/pi/Tesis/cNETmenosFiltros/labels.txt";
    */
    string model_file   = "/home/josejacomeb/Tesis/DatasetMaiz/cNETmenosFiltros/deploy.prototxt";
    string trained_file = "/home/josejacomeb/Tesis/DatasetMaiz/cNETmenosFiltros/maizADAM_iter_9000.caffemodel";
    string mean_file    = "/home/josejacomeb/Tesis/DatasetMaiz/imagenet_mean.binaryproto";
    string label_file   = "/home/josejacomeb/Tesis/DatasetMaiz/labels.txt";

    Classifier classifier(model_file, trained_file, mean_file, label_file);


    /*
    *   Inicio del WiringPi y de la Comunicacion Serial
    */

    //    while(inicializar()){}      //Espera a que se inicialize WiringPi y el Puerto Serial
    //  while(pulsoinicio()){}      //Espera a que se de un pulso de inicio

    VideoCapture video;
    video.open("/media/josejacomeb/688B-D64C/maiz3.avi");
    video.set(CV_CAP_PROP_FPS,15);
    if(!video.isOpened()) {
        std::cout << "No se puede abrir la camara" << std::endl;
        return -1;
    }
    bool pausa = true;

    for(;;){
        std::cout << "Inicio" << std::endl;
        if(pausa){
            video >> imagen;
            if(imagen.empty()){
                break;
            }
        }
        //if(imagenes == 0) imagen = imread("/home/josejacomeb/PruebasCamara/vlcsnap-2017-09-18-23h14m15s762.png");
        //else if(imagenes == 1) imagen = imread("/home/josejacomeb/PruebasCamara/vlcsnap-2017-09-19-10h58m47s717.png");
        //else imagen = imread("/home/josejacomeb/PruebasCamara/vlcsnap-2017-09-19-10h58m31s348.png");
        encontrarcontornos();
        if(contours.size() == 0 ) {
            std::cout << "Sin Contornos Verde" << std::endl;
        }
        else{
            clasificar(classifier);
            generarhilera();
            maicestotales.clear();
            //mandardatos();
        }
        imshow("Generar hilera",p);
        imshow("Dibujo",dibujo);
        waitKey();


    }
    return 0;

}

